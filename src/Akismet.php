<?php
/**
 * @project akismet-php
 * @created 28/08/14
 */

namespace Akismet;

use GuzzleHttp;

class Akismet
{

    /**
     * @var string
     */
    protected $_apiKey;

    /**
     * @var string
     */
    protected $_baseUrl;

    /**
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * @param string $apiKey
     */
    public function __construct($apiKey = null)
    {

        if (!$apiKey) {
            throw new \Exception('You must provide an api key');
        }

        $this->_apiKey  = $apiKey;
        $this->_baseUrl = 'http://' . $this->_apiKey . '.rest.akismet.com/1.1/comment-check';
        $this->client   = new GuzzleHttp\Client([]);
    }

    /**
     * Required keys for data:
     * - `permalink`
     * - `email`
     *
     * Optional keys for data
     * - `author`
     * - `content`
     *
     * @param array $data
     *
     * @throws \Exception
     */
    public function check($data = [])
    {

        if (!isset($data['permalink']) || !isset($data['email'])) {
            throw new \Exception('Missing keys in data array');
        }

        $options = [
            'body' => [
                'blog'                 => !empty($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '',
                'user_ip'              => $_SERVER['REMOTE_ADDR'],
                'user_agent'           => $_SERVER['HTTP_USER_AGENT'],
                'referrer'             => $_SERVER['HTTP_REFERER'],
                'permalink'            => $data['permalink'],
                'comment_type'         => 'form',
                'comment_author'       => isset($data['author']) ? $data['author'] : null,
                'comment_author_email' => $data['email'],
                'comment_author_url'   => null,
                'comment_content'      => !empty($data['content']) ? $data['content'] : null,
            ],
        ];

        try {
            /* @var \Psr\Http\Message\ResponseInterface $response */
            $response = $this->client->post($this->_baseUrl, $options);

            return $response->getBody() == 'true';
        } catch (GuzzleHttp\Exception\ClientException $e) {
            switch ($e->getResponse()->getStatusCode()) {
                default:
                    throw new \Exception(json_decode($e->getResponse()->getBody()->getContents(), true)['error']);
            }
        }
    }

}
