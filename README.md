# README #


## Installing via Composer

```
{
    "require": {
        "vmcms/akismet-php": "~2.0"
    }
}
```


## Usage

```
<?php 

$aksimet = new \Akismet\Akismet('YOURAPIKEY');

$spamCheck = $aksimet->check([
    'permalink' => 'http://www.example.com',
    'email'     => 'some@example.com',
    'content'   => 'your message'
]);

if ($spamCheck) {
    //STOP - Spam detected
} else {
    //All is well
}

```